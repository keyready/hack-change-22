import React, {SetStateAction, useContext, useState} from 'react';
import './styles/App.css';
import {Header} from "../widgets/Header";
import {StreamerPage} from "../pages/StreamerPage";
import {Route, Routes, useLocation} from "react-router-dom";
import {MainPage} from "../pages/MainPage";
import {RegPage} from "../pages/RegPage";
import {LoginPage} from "../pages/LoginPage";
import {CreateEvent} from "../widgets/CreateEvent";
import {DonatePage} from "../pages/DonatePage";


function App() {
    const location = useLocation().pathname

    return (
        <div className="App">
            <Header location={location}/>

            <Routes>
                <Route path='/streamer' element={<StreamerPage/>}/>
                <Route path='/sign_up' element={<RegPage/>}/>
                <Route path='/sign_in' element={<LoginPage/>}/>
                <Route path='/create_event' element={<CreateEvent/>}/>
                <Route path='/sendingPay/:link' element={<DonatePage/>}/>
                <Route path='/*' element={<LoginPage />}/>
            </Routes>
        </div>
    );
}

export default App;
