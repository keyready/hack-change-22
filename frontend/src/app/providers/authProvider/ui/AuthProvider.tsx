import React, {FC, PropsWithChildren, useMemo, useState} from 'react';
import {AuthContext, LOCAL_STORAGE_TOKEN} from "./AuthContext";


const defaultAuth = localStorage.getItem(LOCAL_STORAGE_TOKEN) as string
    || ''

const AuthProvider: FC<PropsWithChildren> = ({children}) => {
    const [token, setToken] = useState<string>(defaultAuth)

    const defaultProps = useMemo(() => ({
        token,
        setToken,
    }), [token]);

    return (
        <AuthContext.Provider value={defaultProps}>
            {children}
        </AuthContext.Provider>
    );
};

export default AuthProvider;