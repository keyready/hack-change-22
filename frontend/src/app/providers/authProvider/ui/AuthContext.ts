import {createContext} from "react";

interface IAuth {
    token: string;
    setToken: (token: string) => void
}

export const AuthContext = createContext<IAuth>({
        token: '',
        setToken: (token: string) => {},
    }
);


export const LOCAL_STORAGE_TOKEN = 'token';
export const LOCAL_STORAGE_DONATION_LINK = 'donationLink';
