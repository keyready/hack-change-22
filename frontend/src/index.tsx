/* Он думает, что если у него мать - мусор, то мне пизда */

import React from 'react';
import ReactDOM from 'react-dom';
import './app/styles/index.css';
import App from './app/App';
import {BrowserRouter} from "react-router-dom";
import AuthProvider from "./app/providers/authProvider/ui/AuthProvider";


ReactDOM.render(
    <AuthProvider>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </AuthProvider>,
    document.getElementById('root')
)