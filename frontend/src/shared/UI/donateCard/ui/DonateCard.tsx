import React, {FC} from 'react';
import './DonateCard.css'

export interface donate {
    id: number;
    author: string;
    value: number;
}

export interface DonateCardProps {
    donate?: donate;
}

export const DonateCard: FC<DonateCardProps> = (props) => {
    const {
        donate
    } = props;

    return (
        <div className='DonateCard'>
            <div className='donator_name'>{donate?.author}</div>
            <div className='donation_amount'>{donate?.value}</div>
        </div>
    );
};