import React, {FC, PropsWithChildren} from 'react';
import './Button.css'

export interface ButtonProps extends PropsWithChildren{
    onClick: () => void;
}

export const Button: FC<ButtonProps> = (props) => {
    const { children, onClick }= props

    return (
        <button onClick={onClick} className='Button'>
            {props.children}
        </button>
    );
};