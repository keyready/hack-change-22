import React, {CSSProperties, FC, PropsWithChildren} from 'react';
import './streamerInfoCard.css'

interface PieProps extends PropsWithChildren{
    width?: string | number;
    height?: string | number;
}

export const StreamerInfoCard: FC<PieProps> = (props) => {
    const {width, height, children} = props

    return (
        <div
            className='Card'
            style={{width, height}}
        >
            {children}
        </div>
    );
};