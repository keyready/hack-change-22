// import React from 'react';
//
//
// interface PieProps {
//     current: number;
// }
//
// export const Pie = ({current}: PieProps) => {
//     return (
//         <div className="pie animate no-round" style={{"--p": {current}, "--c:orange"}}>
//             {current}%
//         </div>
//     );
// };\


import React from 'react';

export const Pie = () => {
    return (
        <div
            style={{
                fontSize: 16,
                width: 150,
                height: 150,
                borderRadius: '50%',
                background: 'green',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
            }}
        >
            Диаграмма
        </div>
    );
};