import React, {FC, useContext} from 'react';
import './Header.css'
// @ts-ignore
import Logo from '../assets/Logo.png'
import {AuthContext} from "../../../app/providers/authProvider";
import {Button} from "../../../shared/UI/Button";
import {useNavigate} from "react-router-dom";


export interface HeaderProps {
    location: string;
}


const Header:FC<HeaderProps> = (props) => {
    const {token, setToken} = useContext(AuthContext)
    const navigate = useNavigate()

    const logout = () => {
        localStorage.clear()
        setToken('')
        navigate('/')
    }

    return (
        <div
            className='Header'
        >
            <img src={Logo} alt=""/>
            <div className='title'>
                { props.location === '/streamer' ? 'Панель стримера' : 'DonationCadets' }
                <hr className='hr'/>
            </div>
            {token
                ? <Button onClick={logout}>Выйти</Button>
                : ''
            }
        </div>
    );
};

export default Header;