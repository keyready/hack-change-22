import React, {FC, PropsWithChildren, useState} from 'react';
import {Link, LinkProps} from "react-router-dom";
import './AppLink.css'


export interface AppLinkProps extends PropsWithChildren, LinkProps {
    className?: string
}


export const AppLink: FC<AppLinkProps> = (props) => {
    const {to, children} = props

    return (
        <Link
            to={to}
            className='AppLink'
        >
            {children}
        </Link>
    );
};