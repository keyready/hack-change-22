import React, {FormEvent, useContext, useEffect, useState} from 'react';
import './CreateEvent.css'
import {StreamerInfoCard} from "../../../shared/UI/streamerInfoCard";
import {AuthContext, LOCAL_STORAGE_DONATION_LINK} from "../../../app/providers/authProvider/ui/AuthContext";
import {Button} from "../../../shared/UI/Button";
import {useNavigate} from "react-router-dom";

interface ICreateEvent {
    title: string;
    description: string;
    amount: number;
}

export const CreateEvent = () => {
    const navigate = useNavigate()
    const [createEvent, setCreateEvent] = useState<ICreateEvent>({title: '', description: '', amount: 0})

    const {token} = useContext(AuthContext)
    if (!token) {
        return (
            <h1 style={{color: 'white'}}>Нет прав</h1>
        )
    }

    const onSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()

        console.log(createEvent)
        fetch('/create_event', {
            method: 'post',
            body: JSON.stringify(createEvent),
            headers: {'authorization': token, 'Content-Type': 'application/json'}
        })
            .then(res => res.json())
            .then(res => {
                localStorage.removeItem(LOCAL_STORAGE_DONATION_LINK)
                const dLink = `sendingPay/${res.donateLink}`;
                localStorage.setItem(LOCAL_STORAGE_DONATION_LINK, dLink)
                navigate('/streamer')
            })
    }


    return (
        <div className='CreateEvent'>
            <StreamerInfoCard
                width='100%'
                height='100%'
            >
                <form className="ui-form" onSubmit={e => onSubmit(e)}>
                    <h3>Добавление таска</h3>
                    <div className="form-row">
                        <input
                            type="text"
                            required
                            autoComplete="off"
                            value={createEvent.title}
                            onChange={e => setCreateEvent({...createEvent, title: e.target.value})}
                        />
                        <label>Название</label>
                    </div>
                    <div className="form-row">
                        <input
                            type="text"
                            required
                            autoComplete="off"
                            value={createEvent.description}
                            onChange={e => setCreateEvent({...createEvent, description: e.target.value})}
                        />
                        <label>Описание</label>
                    </div>
                    <div className="form-row">
                        <input
                            type="number"
                            min={0}
                            required
                            autoComplete="off"
                            value={createEvent.amount}
                            onChange={e => setCreateEvent({...createEvent, amount: ~~e.target.value})}
                        />
                        <label>Сумма</label>
                    </div>
                    <p><input type="submit" value="Создать"/></p>
                    <p><Button onClick={() => navigate('/streamer')}>Назад</Button></p>
                </form>
            </StreamerInfoCard>
        </div>
    );
};