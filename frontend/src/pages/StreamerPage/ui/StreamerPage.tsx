import React, {useContext, useEffect, useState} from 'react';
import './StreamerPage.css'
import {StreamerInfoCard} from "../../../shared/UI/streamerInfoCard";
import {Pie} from "../../../shared/UI/Pie";
import {DonateCard} from "../../../shared/UI/donateCard";
import {donate} from "../../../shared/UI/donateCard/ui/DonateCard";
import {AppLink} from "../../../widgets/AppLink";
import {useClipboard} from "use-clipboard-copy";
import {Button} from "../../../shared/UI/Button";
import {AuthContext} from "../../../app/providers/authProvider";


interface StreamerData {
    name: string;
    surname: string;
    total_amount: number;
    count: number;
    max_price: number;
}

interface TargetDonation {
    title: string;
    description: string;
    amount_collected: number;
    amount_required: number;
    donateLink: string;
    isCompleted: boolean
}


export const StreamerPage = () => {
    const {token} = useContext(AuthContext)
    const buffer = useClipboard()
    const [donates, setDonates] = useState<donate[]>([])
    const [targetDonation, setTargetDonation] = useState<TargetDonation>({
        title: '',
        description: '',
        donateLink: '',
        amount_collected: 0,
        amount_required: 0,
        isCompleted: false
    })
    const [streamer, setStreamer] = useState<StreamerData>({
        name: '',
        surname: '',
        count: 0,
        total_amount: 0,
        max_price: 0
    })


    useEffect(() => {
        fetch('/oneEvent', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'authorization': token
            }
        })
            .then(res => res.json())
            .then(res => {
                setTargetDonation(res)
            })

        fetch('/allEvents', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'authorization': token
            }
        })
            .then(res => res.json())
            .then(res => {
                console.log(res)
                setStreamer(res)
                setDonates(res.rows)
            })
    }, [])

    if (!token) {
        return (
            <h1 style={{color: 'white'}}>Нет прав</h1>
        )
    }

    const dLink = `http://localhost:5000/sendingPay/${targetDonation.donateLink}`
    return (
        <div className='StreamerPage'>
            <input
                type="text"
                hidden={true}
                value={dLink}
                ref={buffer.target}
            />

            <div className="main_info">
                <StreamerInfoCard
                    width={550}
                    height={300}
                >
                    <div className="info">
                        <div className="name">{streamer.name}</div>
                        <div className="surname">{streamer.surname}</div>
                    </div>
                    <div className="balance">{streamer.total_amount}</div>
                    <div className="donations_wrapper">
                        <div className="donations">{streamer.count}</div>
                        <div className="donat_icon"></div>
                    </div>
                    <div className="balance top">{streamer.max_price}</div>
                </StreamerInfoCard>

                <StreamerInfoCard>
                    {targetDonation.isCompleted || !targetDonation.title
                        ? <AppLink to='/create_event'>Добавить ивент</AppLink>
                        : <div>
                            <h3>{targetDonation.title}</h3>
                            <Pie/>
                            <div className="collected">{targetDonation.amount_collected} / {targetDonation.amount_required}</div>
                            <Button
                                onClick={buffer.copy}
                            >
                                Скопировать ссылку
                            </Button>
                        </div>
                    }
                </StreamerInfoCard>
            </div>

            <StreamerInfoCard width='100%'>
                {donates.length
                    ? <div style={{width: '100%'}}>
                        <h2>Последние донаты</h2>
                        <div className='dynamics'>
                            {donates.map(donate =>
                                <DonateCard donate={donate}/>
                            )}
                        </div>
                    </div>
                    : <h2>Загрузка...</h2>
                }
            </StreamerInfoCard>
        </div>
    );
};