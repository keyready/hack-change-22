import React, {FormEvent, useContext, useState} from 'react';
import './LoginPage.css'
import {AuthContext} from "../../../app/providers/authProvider";
import {LOCAL_STORAGE_TOKEN} from "../../../app/providers/authProvider/ui/AuthContext";
import {AppLink} from "../../../widgets/AppLink";
import {useNavigate} from "react-router-dom";

interface LoginData {
    login: string;
    password: string;
}

export const LoginPage = () => {
    const {token, setToken} = useContext(AuthContext)
    const [loginData, setLoginData] = useState<LoginData>({login: '', password: ''})
    const navigate = useNavigate()

    const onSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()

        console.log(loginData)
        fetch('/sign_in', {
            method: 'post',
            body: JSON.stringify(loginData),
            headers: {'Content-Type': 'application/json'}
        })
            .then(res => res.json())
            .then(res => {
                console.log(res)
                const newToken = res.accessToken || ''
                if (!newToken) {
                    if (res.msg === 'passwordInvalid') {
                        console.log('Пароль неверный')
                    }
                    if (res.msg === 'userInvalid') {
                        console.log('Пользователь не найден')
                    }
                }
                setToken(newToken)
                localStorage.setItem(LOCAL_STORAGE_TOKEN, newToken)
                if (newToken) navigate('/streamer')
            })
    }

    return (
        <div className='RegPage'>
            <form className="ui-form" onSubmit={e => onSubmit(e)}>
                <h3>Войти на сайт</h3>
                <div className="form-row">
                    <input
                        type="text"
                        id="email"
                        required
                        autoComplete="off"
                        value={loginData.login}
                        onChange={e => setLoginData({...loginData, login: e.target.value})}
                    />
                    <label htmlFor="email">Email</label>
                </div>
                <div className="form-row">
                    <input
                        type="password"
                        id="password"
                        required
                        autoComplete="off"
                        value={loginData.password}
                        onChange={e => setLoginData({...loginData, password: e.target.value})}
                    />
                    <label htmlFor="password">Пароль</label>
                </div>
                <p><input type="submit" value="Войти"/></p>
                <AppLink style={{marginTop: '20px'}} to='/sign_up'>Нет аккаунта?</AppLink>
            </form>
        </div>
    );
};