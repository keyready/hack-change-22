import React, {FormEvent, useState} from 'react';
import './DonatePage.css'
import {useParams} from "react-router-dom";

interface IDonateInfo {
    author: string;
    title: string;
    description: string;
    card_number: string;
    cvv: string;
    value: number
}

export const DonatePage = () => {
    const {link} = useParams()
    const [donateInfo, setDonateInfo] = useState<IDonateInfo>({
        author: '',
        title: '',
        description: '',
        card_number: '',
        cvv: '',
        value: 0
    })

    const submitDonate = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()

        fetch(`/sendingPay/${link}`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(donateInfo)
        })
            .then(res => res.json())
            .then(res => {
                console.log(res)
            })
    }

    return (
        <form className='form' onSubmit={e => submitDonate(e)}>
            <input type="text" placeholder='Ваше имя' value={donateInfo.author} onChange={e => setDonateInfo({...donateInfo, author: e.target.value})}/>
            <input type="text" placeholder='Название доната' value={donateInfo.title} onChange={e => setDonateInfo({...donateInfo, title: e.target.value})}/>
            <input type="text" placeholder='Сообщение стримеру' value={donateInfo.description} onChange={e => setDonateInfo({...donateInfo, description: e.target.value})}/>
            <input type="text" placeholder='Номер карты' value={donateInfo.card_number} onChange={e => setDonateInfo({...donateInfo, card_number: e.target.value})}/>
            <input type="text" placeholder='CVV' value={donateInfo.cvv} onChange={e => setDonateInfo({...donateInfo, cvv: e.target.value})}/>
            <input type="text" placeholder='Сумма платежа' value={donateInfo.value} onChange={e => setDonateInfo({...donateInfo, value: ~~e.target.value})}/>

            <p><input type="submit" value="Отправить"/></p>
        </form>
    );
};