import React, {FormEvent, useContext, useState} from 'react';
import './RegPage.css'
import {AuthContext} from "../../../app/providers/authProvider";
import {LOCAL_STORAGE_TOKEN} from "../../../app/providers/authProvider/ui/AuthContext";
import {AppLink} from "../../../widgets/AppLink";
import {useNavigate} from "react-router-dom";

interface RegData {
    name: string;
    surname: string;
    login: string;
    password: string;
}

export const RegPage = () => {
    const {token, setToken} = useContext(AuthContext)
    const navigate = useNavigate()
    const [loginData, setLoginData] = useState<RegData>({login: '', password: '', surname: '', name: ''})

    const onSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()

        console.log(loginData)
        await fetch('/sign_up', {
            method: 'post',
            body: JSON.stringify(loginData),
            headers: {'Content-Type': 'application/json'}
        })
            .then(res => res.json())
            .then(res => {
                console.log(res)
                const newToken = res.accessToken || ''
                if (!newToken && res.msg === 'userLoginAlreadyExisted') {
                    console.log('Пользователь уже существует')
                }
                setToken(newToken)
                localStorage.setItem(LOCAL_STORAGE_TOKEN, newToken)
                if (newToken) navigate('/streamer')
            })
    }

    return (
        <div className='RegPage'>
            <form className="ui-form" onSubmit={e => onSubmit(e)}>
                <h3>Регистрация стримера</h3>
                <div className="form-row">
                    <input
                        type="text"
                        required
                        autoComplete="off"
                        value={loginData.name}
                        onChange={e => setLoginData({...loginData, name: e.target.value})}
                    />
                    <label>Имя</label>
                </div>
                <div className="form-row">
                    <input
                        type="text"
                        required
                        autoComplete="off"
                        value={loginData.surname}
                        onChange={e => setLoginData({...loginData, surname: e.target.value})}
                    />
                    <label>Фамилия</label>
                </div>
                <div className="form-row">
                    <input
                        type="text"
                        required
                        autoComplete="off"
                        value={loginData.login}
                        onChange={e => setLoginData({...loginData, login: e.target.value})}
                    />
                    <label>Логин</label>
                </div>
                <div className="form-row">
                    <input
                        type="password"
                        required
                        autoComplete="off"
                        value={loginData.password}
                        onChange={e => setLoginData({...loginData, password: e.target.value})}
                    />
                    <label>Пароль</label>
                </div>
                <p><input type="submit" value="Зарегистрироваться"/></p>
                <AppLink style={{marginTop: '20px'}} to='/sign_in'>Уже есть аккаунт?</AppLink>
            </form>
        </div>
    );
};