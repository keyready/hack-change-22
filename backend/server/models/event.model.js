const DB = require('../config/db.connect');
const {DataTypes} = require('sequelize');

module.exports = DB.define('events',{
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    description:{
        type:DataTypes.STRING,
        allowNull:false
    },
    amount_required:{
        type:DataTypes.INTEGER,
        allowNull:false
    },
    amount_collected:{
        type:DataTypes.INTEGER,
        defaultValue:0,
        allowNull:false
    },
    isCompleted: {
        type:DataTypes.BOOLEAN,
        defaultValue: false
    },
    donateLink:{
        type:DataTypes.STRING,
        allowNull:false
    }
},{
    timestamps:false,
    tableName:'Events'
})
