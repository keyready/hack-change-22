const DB = require('../config/db.connect');
const {DataTypes} = require('sequelize');

module.exports = DB.define('streamers', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    surname: {
        type: DataTypes.STRING,
        allowNull: false
    },
    login: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    balance: {
        type: DataTypes.BIGINT,
        allowNull: false,
        defaultValue: 0
    },
    topDonation:{
        type:DataTypes.BIGINT,
        defaultValue: 0,
        allowNull:true
    },
    quantityDonations:{
        type:DataTypes.INTEGER,
        defaultValue: 0,
        allowNull:true
    }
}, {
    timestamps: false,
    tableName: 'Streamers'
})