const DB = require('../config/db.connect');
const {DataTypes} = require('sequelize');

module.exports = DB.define('tokens',{
    refreshToken:{
        type:DataTypes.STRING
    }
},{
    tableName:'Token',
    timestamps:false
})