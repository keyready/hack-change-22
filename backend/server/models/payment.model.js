const DB = require('../config/db.connect');
const {DataTypes} = require('sequelize');

let d = new Date();
let dateString = d.getDate()  + "-" + (d.getMonth()+1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes();

module.exports = DB.define('payments',{
    author:{
        type:DataTypes.STRING,
        allowNull:false
    },
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    description:{
        type:DataTypes.STRING,
        allowNull:false
    },
    cart_number:{
        type:DataTypes.STRING,
        allowNull:false
    },
    cvv:{
        type:DataTypes.STRING,
        allowNull:false
    },
    value:{
        type:DataTypes.INTEGER,
        allowNull:false
    },
    event_time:{
        type:DataTypes.STRING,
        value:`${dateString}`
    },
},{
    timestamps:false,
    tableName:'Payments'
})