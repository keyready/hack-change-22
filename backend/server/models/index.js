const Streamer = require('../models/user.model');
const Payment = require('../models/payment.model');
const Token = require('../models/token.model');
const Event = require('../models/event.model');

Streamer.hasOne(Token);

Streamer.hasMany(Payment);

Streamer.hasMany(Event);

Event.hasMany(Payment);

module.exports = {
    Streamer,Payment,Token,Event
}