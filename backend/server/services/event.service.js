const {Event, Payment, Streamer} = require('../models')
const {Sequelize} = require('sequelize')

class EventService {
    async createEvent(title, description, amount_required, donateLink, streamerId) {
        const event = await Event.create({
            title, description, amount_required, donateLink, streamerId
        })
        return event
    }

    async sendingPay(cart_number, cvv, value, title, description, donateLink, author) {
        const event = await Event.findOne({where: {donateLink}});

        const payment = await Payment.create({
            cart_number, cvv, value, title, description, eventId: event.id, streamerId: event.streamerId, author
        })

        if (event.amount_required > event.amount_collected) {
            console.log('просто платеж')
            event.amount_collected += payment.value;
            await event.save()
        }
        if (event.amount_required <= event.amount_collected) {
            console.log('Выполнено')
            event.isCompleted = true;
            await event.save()
        }

        return {msg: 'платеж прошел'}
    }

    async outputAllCash(streamerId) {
        const event = await Event.findOne({where: {streamerId: streamerId}});
        const streamer = await Streamer.findByPk(streamerId);
        if (event.amount_required) {
            streamer.balance = event.amount_required
            await streamer.save()
            return `Вывод средств успешнно совершен.Ваш баланс ${streamer.balance}`
        } else {
            return 'Пустой баланс'
        }
    }


    async showAllEvents(streamerId) {
        const queryParams = {
            where: {
                streamerId: streamerId
            },
            order: [
                ['value', 'DESC']
            ],
            attributes: [
                'value',
                "author",
            ],
            raw: true
        }
        const {count, rows} = await Payment.findAndCountAll(queryParams);
        const streamer = await Streamer.findByPk(streamerId)
        const max_donate = rows[0]
        const total_amount = rows.map(item => item.value).reduce((prev, curr) => prev + curr, 0);

        return {
            total_amount,
            count,
            max_price: max_donate.value,
            name: streamer.name, surname: streamer.surname,
            rows
        }
    }

    async showOneEvent(streamerId) {
        const arrayEvents = await Event.findAll({where: {streamerId}, raw: true})
        const currentEvent = arrayEvents[arrayEvents.length - 1]
        return currentEvent
    }

}

module.exports = new EventService();