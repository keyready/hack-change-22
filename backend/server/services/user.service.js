const TokenService = require('../services/token.service');
const bcrypt = require('bcrypt');
const UserDTO = require('../dto/user.dto');
const {Streamer, Token} = require('../models');


class UserService {

    async sing_up(name, surname, login, password) {
        const candidate = await Streamer.findOne({where: {login}})
        if (candidate) {
            return {msg: 'userLoginAlreadyExisted'}
        }

        const hashPassword = await bcrypt.hash(password, 5)

        const streamer = await Streamer.create({
            name, surname, login,
            password: hashPassword
        })

        const streamerData = new UserDTO(streamer);
        const tokens = await TokenService.generateToken({...streamerData})
        await TokenService.saveToken(streamerData.id, tokens.refreshToken)
        return {streamerData, ...tokens}
    }

    async sing_in(login, password) {
        const candidate = await Streamer.findOne({where: {login}})
        if (!candidate) {
            return {msg: 'userInvalid'}
        }
        const checkPassword = await bcrypt.compare(password, candidate.password)
        if (!checkPassword) {
            return {msg: 'passwordInvalid'}
        }
        const streamerData = new UserDTO(candidate);
        const tokens = await TokenService.generateToken({...streamerData});
        await TokenService.saveToken(streamerData.id, tokens.refreshToken)

        return {streamerData, ...tokens}
    }

    async refresh(userId) {
        const refreshToken = await Token.findOne({where: {userId: userId}})
        if (!refreshToken) {
            return {msg: 'Неавторизован'}
        }
        const streamerData = TokenService.validateRefreshToken(refreshToken);
        const tokenFromDB = await TokenService.findToken(refreshToken);
        if (!streamerData || !tokenFromDB) {
            return {msg: 'Неавторизован'}
        }
        const user = await Streamer.findByPk(streamerData.id);
        const streamerDTO = new UserDTO(user);
        const tokens = TokenService.generateToken({...streamerDTO});

        await TokenService.saveToken(streamerDTO.id, tokens.refreshToken);
        return {...tokens, streamerDTO}
    }

    async logout(refreshToken) {
        const token = await TokenService.removeToken(refreshToken)
        return token;
    }

    async showOneUser(userId) {
        const user = await Streamer.findByPk(userId)
        return user;
    }

}

module.exports = new UserService()