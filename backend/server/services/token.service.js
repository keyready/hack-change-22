const jwt = require('jsonwebtoken');
const {Token} = require('../models')

class TokenService{

    async generateToken(payload){
        const accessToken = jwt.sign(payload,process.env.JWT_ACCESS_SECRET,{expiresIn: '30m'});
        const refreshToken = jwt.sign(payload,process.env.JWT_REFRESH_SECRET,{expiresIn: '30d'})
        return {accessToken, refreshToken}
    }

    async saveToken(userId,refreshToken){
        const token = await Token.findOne({where:{refreshToken}})
        if(token){
           token.refreshToken = refreshToken
           token.save()
        }
        else {
            const token = await Token.create({refreshToken:refreshToken,userId:userId})
            return token
        }
    }

    async removeToken(refreshToken){
        const oldToken = await Token.destroy({where:{refreshToken}})
        return oldToken
    }

    async validateAccessToken(accessToken){
        const validateData = jwt.verify(accessToken,process.env.JWT_ACCESS_SECRET)
        return validateData
    }

    async validateRefreshToken(refreshToken){
        const validateData = jwt.sign(refreshToken,process.env.JWT_REFRESH_SECRET)
        return validateData
    }

    async findToken(refreshToken){
        const token = await Token.findOne({where:{refreshToken}})
        return token
    }

}

module.exports = new TokenService();