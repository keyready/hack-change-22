const UserService = require('../services/user.service');

class UserController {

    async sing_up(req, res, next) {
        try {
            const {name, surname, login, password} = req.body;
            const newUser = await UserService.sing_up(name, surname, login, password);
            res.cookie('refreshToken', newUser.refreshToken, {maxAge: 30 * 24 * 3600 * 1000, httpOnly: true})
            return res.json(newUser)
        } catch (e) {
            next(e.message)
            console.log(e.message)
        }
    }

    async sing_in(req, res, next) {
        try {
            const {login,password} = req.body;
            const User = await UserService.sing_in(login,password)
            res.cookie('refreshToken', User.refreshToken, {maxAge: 30 * 24 * 3600 * 1000, httpOnly: true})
            return res.json(User)
        } catch (e) {
            next(e.message)
            console.log(e.message)
        }
    }

    async logout(req, res, next) {
        try {
            const {refreshToken} = req.cookie;
            const token = await UserService.logout(refreshToken);
            res.clearCookie('refreshToken')
            return res.json(token)
        } catch (e) {
            next(e.message)
            console.log(e.message)
        }
    }

    async refresh(req, res, next) {
        try {
            const {refreshToken} = req.cookie;
            const token = await UserService.refresh(refreshToken);
            res.cookies('refreshToken', token.refreshToken, {maxAge: 30 * 24 * 3600 * 1000, httpOnly: true});
            return res.json(token.accessToken);
        } catch (e) {
            next(e.message);
            console.log(e.message)
        }
    }

    async showOneUser(req,res,next){
        try{
            const userId = await req.user.id
            const user = await UserService.showOneUser(userId)
            return res.json(user)
        }
        catch (e){
            next(e.message)
            console.log(e.message)
        }
    }



}

module.exports = new UserController();