const EventService = require('../services/event.service');
const uuid = require('uuid');

class EventController {

    async createEvent(req, res, next) {
        try {
            const {title, description, amount} = req.body;
            const donateLink = uuid.v4()
            const {id} = await req.user
            const event = await EventService.createEvent(title, description, amount, donateLink, id);
            return res.json(event)
        } catch (e) {
            console.log(e.message)
            next(e.message)
        }
    }

    async sendingPay(req, res, next) {
        try {
            const {card_number, cvv, value, title, description, author} = req.body;
            const {link} = req.params;
            console.log(req)
            const infoPay = await EventService.sendingPay(
                card_number, cvv, value, title, description, link, author
            );
            return res.json({msg: infoPay})
        } catch (e) {
            next(e.message)
            console.log(e.message)
        }
    }

    async outputCash(req, res, next) {
        try {
            const streamerId = await req.user.id;
            const infoOutput = await EventService.outputAllCash(streamerId);
            return res.json({msg: infoOutput})
        } catch (e) {
            next(e.message)
            console.log(e.message)
        }
    }

    async showAllEvents(req, res, next) {
        try {
            console.log('Показать все')
            const {id} = await req.user
            const events = await EventService.showAllEvents(id);
            console.log(events)
            return res.json(events)
        } catch (e) {
            next(e.message)
            console.log(e.message)
        }
    }

    async showOneEvent(req, res, next) {
        try {
            const {id} = await req.user;
            const event = await EventService.showOneEvent(id)
            return res.json(event)
        } catch (e) {
            next(e.message)
            console.log(e.message)
        }
    }


}

module.exports = new EventController()