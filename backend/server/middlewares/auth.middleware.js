const TokenService = require('../services/token.service');

module.exports = (req, res, next) => {
    try {
        const accessToken = req.headers.authorization;
        if (!accessToken) {
            return res.json({msg: 'Неавторизованный запрос', status: 401})
        }
        const userData = TokenService.validateAccessToken(accessToken);
        console.log(userData)
        req.user = userData;
        next()
    } catch (e) {
        next(e.message)
        console.log(e.message)
    }
}