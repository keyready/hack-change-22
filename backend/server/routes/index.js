const UserRoutes = require('./user.routes');
const EventRoutes = require('./event.routes');

module.exports = {
    EventRoutes:EventRoutes,
    UserRoutes:UserRoutes
}