const EventControllers = require('../controllers/event.controller');
const Middleware = require('../middlewares/auth.middleware');

module.exports = (app) => {

    app.post('/create_event', [Middleware], EventControllers.createEvent)

    app.post('/sendingPay/:link', EventControllers.sendingPay)

    app.post('/outputCash', [Middleware], EventControllers.outputCash)

    app.post('/allEvents', [Middleware], EventControllers.showAllEvents)

    app.post('/oneEvent', [Middleware], EventControllers.showOneEvent)
}