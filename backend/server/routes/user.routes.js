const UserControllers = require('../controllers/user.controller');

module.exports = (app) => {

    app.post('/sign_up', UserControllers.sing_up)

    app.post('/sign_in', UserControllers.sing_in)

    app.post('/logout', UserControllers.logout)

    app.post('/refresh', UserControllers.refresh)

    app.post('/oneUser',UserControllers.showOneUser)
}