require('dotenv').config();
const express = require('express');
const app = express();
const DB = require('./server/config/db.connect');
const RouterApp = require('./server/routes');
const CookieParser = require('cookie-parser');

const PORT = process.env.PORT || 4321
const path = require('path');

app.use(express.static(path.resolve('../front/build/')));
app.use(express.json());
app.use(CookieParser());

RouterApp.UserRoutes(app);
RouterApp.EventRoutes(app);

const StartApp = async () =>{
    try{
        await DB.sync();
        // await DB.sync({force: true});
        await app.listen(PORT,() =>{
            app.get('/*',(req,res) =>{
                res.sendFile(path.resolve('../front/build/index.html'))
            })
            console.log(`http://localhost:${PORT}`)
        })
    }
    catch(e){
        console.log(e.message)
    }
}

StartApp();